import keyMirror from 'key-mirror'

export default keyMirror({
    ADD_TODO: null
})