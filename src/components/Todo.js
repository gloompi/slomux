import React, {Component} from 'react'
import Slomux from '../Slomux'

import addTodo from 'actions'

class ToDoComponent extends Component {
  state = {
    todoText: ''
  }

  render() {
    return (
      <div>
        <label>{this.props.title || 'Без названия'}</label>
        <div>
          <input
            value={this.state.todoText}
            placeholder="Название задачи"
            onChange={this.updateText}
          />
          <button onClick={this.addTodo}>Добавить</button>
          <ul>
            {this.props.todos.map((todo, idx) => <li key={idx}>{todo}</li>)}
          </ul>
        </div>
      </div>
    )
  }

  updateText = ev => {
    const { value } = ev.target

    this.setState({
      todoText: value
    })
  }

  addTodo = () => {
    this.props.addTodo(this.state.todoText)

    this.state.todoText = ''
  }
}

export default Slomux.connect(state => ({
  todos: state,
}), dispatch => ({
  addTodo: text => dispatch(addTodo(text)),
}))(ToDoComponent)