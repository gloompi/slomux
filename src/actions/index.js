import constants from 'constants'

const addTodo = todo => ({
	type: constants.ADD_TODO,
	payload: todo,
})

export default addTodo