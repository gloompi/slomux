import React, {Component} from 'react'

class Provider extends Component {
	componentWillMount() {
		window.store = this.props.store
	}
	
	render() {
		return this.props.children
	}
}

let Slomux = {
	createStore: (reducer, initialState) => {
		let currentState = initialState
		const listeners = []
	
		const getState = () => currentState
		const dispatch = action => {
			currentState = reducer(currentState, action)
			listeners.forEach(listener => listener())
		}
	
		const subscribe = listener => listeners.push(listener)
	
		return { getState, dispatch, subscribe }
	},
	connect: (mapStateToProps, mapDispatchToProps) => Component => {
		return class Connected extends Component {
			render() {
				return (
					<Component
						{...mapStateToProps(store.getState(), this.props)}
						{...mapDispatchToProps(store.dispatch, this.props)}
					/>
				)
			}
	
			componentDidMount() {
				store.subscribe(this.handleChange)
			}
	
			handleChange = () => {
				this.forceUpdate()
			}
		}
	},
	Provider: Provider
}
 

export default Slomux