import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Slomux from '../Slomux'
import ToDo from 'components/Todo'
import store from 'store'

export default class App extends Component{
  static propTypes = {
  }
  render(){
    const Provider = Slomux.Provider
    return(
      <Provider store={store}>
        <ToDo title="Список задач"/>
      </Provider>
    )
  }
}