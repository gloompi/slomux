import Slomux from '../Slomux'

import reducer from 'reducers'

const createStore = Slomux.createStore

const store = createStore(reducer, [])

export default store