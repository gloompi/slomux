import React from 'react'
import { render } from 'react-dom'
import 'assets/styles/styles.scss'
import App from 'containers/App'
import { AppContainer } from 'react-hot-loader'


const renderApp = Component => {
	render(
		<AppContainer>
			<App />
		</AppContainer>,
		document.querySelector('#mount_place')
	)
}

renderApp(renderApp)

if(module.hot) {
	module.hot.accept('containers/App', () => { renderApp(App) })
}
